
// DlgProxy.h: header file
//

#pragma once

class CfunctionDlg;


// CfunctionDlgAutoProxy command target

class CfunctionDlgAutoProxy : public CCmdTarget
{
	DECLARE_DYNCREATE(CfunctionDlgAutoProxy)

	CfunctionDlgAutoProxy();           // protected constructor used by dynamic creation

// Attributes
public:
	CfunctionDlg* m_pDialog;

// Operations
public:

// Overrides
	public:
	virtual void OnFinalRelease();

// Implementation
protected:
	virtual ~CfunctionDlgAutoProxy();

	// Generated message map functions

	DECLARE_MESSAGE_MAP()
	DECLARE_OLECREATE(CfunctionDlgAutoProxy)

	// Generated OLE dispatch map functions

	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

