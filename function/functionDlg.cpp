
// functionDlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "function.h"
#include "functionDlg.h"
#include "DlgProxy.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
  CAboutDlg();

  // Dialog Data
#ifdef AFX_DESIGN_TIME
  enum { IDD = IDD_ABOUTBOX };
#endif

protected:
  virtual void DoDataExchange(CDataExchange *pDX);    // DDX/DDV support

// Implementation
protected:
  DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
  EnableActiveAccessibility();
}

void CAboutDlg::DoDataExchange(CDataExchange *pDX)
{
  CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CfunctionDlg dialog


IMPLEMENT_DYNAMIC(CfunctionDlg, CDialogEx);

CfunctionDlg::CfunctionDlg(CWnd *pParent /*=nullptr*/)
  : CDialogEx(IDD_FUNCTION_DIALOG, pParent)
{
  EnableActiveAccessibility();
  m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
  m_pAutoProxy = nullptr;
}

CfunctionDlg::~CfunctionDlg()
{
  // If there is an automation proxy for this dialog, set
  //  its back pointer to this dialog to null, so it knows
  //  the dialog has been deleted.
  if (m_pAutoProxy != nullptr)
    m_pAutoProxy->m_pDialog = nullptr;
}

void CfunctionDlg::DoDataExchange(CDataExchange *pDX)
{
  CDialogEx::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_EDITFUNCTION, poly_edit_);
  DDX_Control(pDX, IDC_LISTMSG, msg_list_);
  DDX_Control(pDX, IDC_BUTTONSETVAR, button_set_var_);
  DDX_Control(pDX, IDC_MFCBUTTONCALC, button_calc_);
}

BEGIN_MESSAGE_MAP(CfunctionDlg, CDialogEx)
  ON_WM_SYSCOMMAND()
  ON_WM_CLOSE()
  ON_WM_PAINT()
  ON_WM_QUERYDRAGICON()
  ON_WM_MOVE()
  ON_BN_CLICKED(IDOK, &CfunctionDlg::OnBnClickedOk)
  ON_BN_CLICKED(IDC_BUTTONSETVAR, &CfunctionDlg::OnBnClickedButtonsetvar)
  ON_BN_CLICKED(IDC_MFCBUTTONCALC, &CfunctionDlg::OnBnClickedMfcbuttoncalc)
  ON_EN_CHANGE(IDC_EDITFUNCTION, &CfunctionDlg::OnChangeEditfunction)
END_MESSAGE_MAP()


// CfunctionDlg message handlers

BOOL CfunctionDlg::OnInitDialog()
{
  CDialogEx::OnInitDialog();

  // Add "About..." menu item to system menu.

  // IDM_ABOUTBOX must be in the system command range.
  ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
  ASSERT(IDM_ABOUTBOX < 0xF000);

  CMenu *pSysMenu = GetSystemMenu(FALSE);
  if (pSysMenu != nullptr)
  {
    BOOL bNameValid;
    CString strAboutMenu;
    bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
    ASSERT(bNameValid);
    if (!strAboutMenu.IsEmpty())
    {
      pSysMenu->AppendMenu(MF_SEPARATOR);
      pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
    }
  }

  // Set the icon for this dialog.  The framework does this automatically
  //  when the application's main window is not a dialog
  SetIcon(m_hIcon, TRUE);			// Set big icon
  SetIcon(m_hIcon, FALSE);		// Set small icon

  // TODO: Add extra initialization here
  SetEditFont();

  return TRUE;  // return TRUE  unless you set the focus to a control
}

void CfunctionDlg::SetEditFont()
{
  LOGFONTW logfont;
  memset(&logfont, 0, sizeof(LOGFONTW));
  logfont.lfHeight = 0;
  logfont.lfWidth =  0;
  logfont.lfEscapement = 0;
  logfont.lfOrientation = 0;
  logfont.lfWeight = FW_NORMAL;
  logfont.lfItalic = FALSE;
  logfont.lfUnderline = FALSE;
  logfont.lfStrikeOut = FALSE;
  logfont.lfCharSet = GB2312_CHARSET;
  logfont.lfCharSet = DEFAULT_CHARSET;
  logfont.lfOutPrecision = OUT_DEFAULT_PRECIS;
  logfont.lfClipPrecision = CLIP_DEFAULT_PRECIS;
  logfont.lfQuality = CLEARTYPE_QUALITY;
  logfont.lfPitchAndFamily = DEFAULT_PITCH;
  wcsncpy_s(logfont.lfFaceName, L"Consolas", 8);

  edit_font_.CreateFontIndirectW(&logfont);
  CEdit *edit{ (CEdit *)GetDlgItem(IDC_EDITFUNCTION) };
  if (edit != nullptr) {
    edit->SetFont(&edit_font_);
  }
  CListBox *list_box{ (CListBox *)GetDlgItem(IDC_LISTMSG) };
  if (list_box != nullptr) {
    list_box->SetFont(&edit_font_);
  }
}

void CfunctionDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
  if ((nID & 0xFFF0) == IDM_ABOUTBOX)
  {
    CAboutDlg dlgAbout;
    dlgAbout.DoModal();
  }
  else
  {
    CDialogEx::OnSysCommand(nID, lParam);
  }
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CfunctionDlg::OnPaint()
{
  if (IsIconic())
  {
    CPaintDC dc(this); // device context for painting

    SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

    // Center icon in client rectangle
    int cxIcon = GetSystemMetrics(SM_CXICON);
    int cyIcon = GetSystemMetrics(SM_CYICON);
    CRect rect;
    GetClientRect(&rect);
    int x = (rect.Width() - cxIcon + 1) / 2;
    int y = (rect.Height() - cyIcon + 1) / 2;

    // Draw the icon
    dc.DrawIcon(x, y, m_hIcon);
  }
  else
  {
    CDialogEx::OnPaint();
  }
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CfunctionDlg::OnQueryDragIcon()
{
  return static_cast<HCURSOR>(m_hIcon);
}

// Automation servers should not exit when a user closes the UI
//  if a controller still holds on to one of its objects.  These
//  message handlers make sure that if the proxy is still in use,
//  then the UI is hidden but the dialog remains around if it
//  is dismissed.

void CfunctionDlg::OnClose()
{
  if (CanExit())
    CDialogEx::OnClose();
}

void CfunctionDlg::OnOK()
{
  if (CanExit())
    CDialogEx::OnOK();
}

void CfunctionDlg::OnCancel()
{
  if (CanExit())
    CDialogEx::OnCancel();
}

BOOL CfunctionDlg::CanExit()
{
  // If the proxy object is still around, then the automation
  //  controller is still holding on to this application.  Leave
  //  the dialog around, but hide its UI.
  if (m_pAutoProxy != nullptr)
  {
    ShowWindow(SW_HIDE);
    return FALSE;
  }

  return TRUE;
}



void CfunctionDlg::OnMove(int x, int y)
{
  CDialogEx::OnMove(x, y);

  // TODO: Add your message handler code here
  CWnd *dialog_item{ GetDlgItem(IDC_EDITFUNCTION) };

  RECT rect;
  GetClientRect(&rect); // right 就是窗口的宽度。

  if (dialog_item != nullptr) {
    dialog_item->MoveWindow(110, 47, rect.right - 120, 26);
  }

  if ((dialog_item = GetDlgItem(IDC_LISTMSG)) != nullptr) {
    dialog_item->MoveWindow(10, 80, rect.right - 20, rect.bottom - 135);
  }

  if ((dialog_item = GetDlgItem(IDOK)) != nullptr) {
    dialog_item->MoveWindow(50, rect.bottom - 46, 138, 30);
  }


  if ((dialog_item = GetDlgItem(IDC_BUTTONSETVAR)) != nullptr) {
    dialog_item->MoveWindow(50 + 138 + 80, rect.bottom - 46, 100, 30);
  }


  if ((dialog_item = GetDlgItem(IDC_MFCBUTTONCALC)) != nullptr) {
    dialog_item->MoveWindow(50 + 138 + 80 + 100 + 80, rect.bottom - 46, 80, 30);
  }

  if ((dialog_item = GetDlgItem(IDCANCEL)) != nullptr) {
    dialog_item->MoveWindow(50 + 138 + 80 + 100 + 80 + 80 + 80, rect.bottom - 46, 80, 30);
  }

}

void CfunctionDlg::OnBnClickedOk()
{
  // TODO: Add your control notification handler code here
  //CDialogEx::OnOK();
  // 设置函数表达式
  CString polystr;

  poly_edit_.GetWindowTextW(polystr);
  bool enable{ poly_.set_poly(polystr.GetBuffer(), polystr.GetLength()) };
  button_set_var_.EnableWindow(enable && poly_.nvars() != 0);
  button_calc_.EnableWindow(enable && poly_.nvars() == 0);
  show_dump();
  polystr.ReleaseBuffer();
}

void CfunctionDlg::show_dump()
{
  // 显示详细信息
  msg_list_.ResetContent();

#if 0
  const wchar_t *p, *q;

  const std::wstring &msg{ poly_.dump() };

  q = msg.c_str() - 1;
  while (q) {
    if ((p = wcschr(++q, L'\n')) != nullptr) {
      msg_list_.AddString(msg.substr(q - msg.c_str(), p - q).c_str());
    }
    q = p;
  }
#endif // 0
  wchar_t *p, *q;
  const std::wstring &msg{ poly_.dump() };
  wchar_t *buf{ new wchar_t[msg.size() + 1] };

  if (buf == nullptr)
    return;

  wcsncpy_s(buf, msg.size() + 1, msg.c_str(), msg.size());
  msg_list_.LockWindowUpdate();
  q = buf - 1;
  while (q) {
    if ((p = wcschr(++q, L'\n')) != nullptr) {
      *p = L'\0';
    }
    msg_list_.AddString(q);
    q = p;
  }
  delete[] buf;

  // 使新加的项目可见
  msg_list_.SetCurSel(msg_list_.GetCount() - 1);
  // 取消选中最后一个。
  msg_list_.SetCurSel(-1);
  msg_list_.UnlockWindowUpdate();
}

void CfunctionDlg::OnBnClickedButtonsetvar()
{
  // TODO: Add your control notification handler code here
   // 设置变量
  CFileDialog file_dialog(TRUE);//, "*.*\0\0", NULL, OFN_HIDEREADONLY, "*.*\0\0");

  if (poly_.nvars() == 0)
    return;

  if (poly_.nvars() == 0 || file_dialog.DoModal() != IDOK)
    return;

  FILE *fp{ nullptr };

  if(_wfopen_s(&fp, (LPCWSTR)file_dialog.GetPathName(), L"rt") != 0 || fp == nullptr) {
    MessageBoxW((LPCWSTR)file_dialog.GetPathName(), L"打开文件出错", MB_OK);
    return;
  }

  double *vars{ new double[poly_.nvars()] };
  memset(vars, 0, sizeof(double) * poly_.nvars());

  int i{ 0 };
  while (i < poly_.nvars()) {
    if (fwscanf_s(fp, L"%lg", &vars[i]) == 1)
      i++;
    else
      break;
  }
  if (i < poly_.nvars() || !poly_.set_vars(vars, poly_.nvars())) {
    MessageBoxW(L"设置变量出错", L"设置变量出错", MB_OK);
    button_calc_.EnableWindow(false);
  }
  else {
    button_calc_.EnableWindow(true);
    show_dump();
  }
  delete[] vars;
  fclose(fp);
}


void CfunctionDlg::OnBnClickedMfcbuttoncalc()
{
  // TODO: Add your control notification handler code here
  // 计算函数表达式的值
  wchar_t buf[128]{ 0 };

  _snwprintf_s(buf, _countof(buf), _countof(buf) - 1, L"计算的结果： %.*lg", DBL_DIG, poly_.calc());
  msg_list_.LockWindowUpdate();
  msg_list_.AddString(buf);
  // 使新加的项目可见
  msg_list_.SetCurSel(msg_list_.GetCount() - 1);
  // 取消选中最后一个。
  msg_list_.SetCurSel(-1);

  msg_list_.UnlockWindowUpdate();
}



void CfunctionDlg::OnChangeEditfunction()
{
  // TODO:  If this is a RICHEDIT control, the control will not
  // send this notification unless you override the CDialogEx::OnInitDialog()
  // function and call CRichEditCtrl().SetEventMask()
  // with the ENM_CHANGE flag ORed into the mask.

  // TODO:  Add your control notification handler code here
  button_set_var_.EnableWindow(true);
  button_calc_.EnableWindow(true);
}
