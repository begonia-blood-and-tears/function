
// functionDlg.h : header file
//

#pragma once

#include "base/poly.h"

class CfunctionDlgAutoProxy;


// CfunctionDlg dialog
class CfunctionDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CfunctionDlg);
	friend class CfunctionDlgAutoProxy;

private:
	CFont edit_font_;
	CEdit poly_edit_;
	CListBox msg_list_;
	CButton button_set_var_;
	CMFCButton button_calc_;
	Poly poly_;
	void SetEditFont();
	void show_dump();

// Construction
public:
	CfunctionDlg(CWnd* pParent = nullptr);	// standard constructor
	virtual ~CfunctionDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_FUNCTION_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	CfunctionDlgAutoProxy* m_pAutoProxy;
	HICON m_hIcon;

	BOOL CanExit();

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	virtual void OnOK();
	virtual void OnCancel();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnMove(int x, int y);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButtonsetvar();
	afx_msg void OnBnClickedMfcbuttoncalc();
	afx_msg void OnChangeEditfunction();
};
