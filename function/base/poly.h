#ifndef __POLY_H__
#define __POLY_H__
#pragma once

#include <cmath>
#include <cstdarg>
#include <cstdlib>
#include <ctype.h>
#include <cstring>
#include <string>

#include "list.h"
#include "stack.h"

/*
double  asin  (double);             / * ASIN  * /
double  sin   (double);             / * SIN   * /
double  sinh  (double);             / * SINH  * /
double  acos  (double);             / * ACOS  * /
double  cos   (double);             / * COS   * /
double  cosh  (double);             / * COSH  * /
double  atan  (double);             / * ATAN  * /
double  atan2 (double, double);     / * ATAN2 * /
double  tan   (double);             / * TAN   * /
double  tanh  (double);             / * TANH  * /
double  pow   (double, double);     / * POW   * /
double  sqrt  (double);             / * SQRT  * /
double  log   (double);             / * LOG   * /
double  log10 (double);             / * LOG10 * /
double  fabs  (double);             / * FABS  * /
double  exp   (double);             / * EXP   * /
*/

typedef enum {IDFUNC, IDOPT, IDVAR, IDCONST} IDType;
            // +    -    *    /    %    (    )      ,
typedef enum {ADD, SUB, MUL, DIV, REM, LBRA, RBRA, COMMA, OPTS} Opt; // COMMA是一个特殊的操作符，它不参与运算; OPTS:操作符个数

typedef enum {ASIN, SIN, SINH, ACOS, COS, COSH, ATAN, ATAN2, TAN, TANH, POW,
              SQRT, LOG, LOG10, FABS, EXP, FUNCS} Func; //FUNCS:函数个数
typedef enum {ALLRIGHT, IDERROR, FUNCERROR, VARERROR, CHERROR,
              OPTERROR, LBRAERROR, RBRAERROR, POLYEMPTY, IDTOLONG} PError;

enum {KEYSIZE = 33}; // 标识符最多可以长 32 个字节

struct FuncTable {
  wchar_t id[KEYSIZE];
  Func func;
};

struct OptTable {
  wchar_t id;
  Opt opt;
};

struct PriTable {
  Opt opt;
  int inpri;
  int outpri;
};

struct Vars {
  wchar_t id[KEYSIZE];
  int varaddr;
  int sign;
  double value;
};

struct Node {
  IDType type;        // 类型：函数、操作符、变量、常量
  union {
    struct {
      int sign;
      int varaddr;                  // 变量
    } var;
    struct {
      int sign;
      Func func;
    } func;                         // 函数
    Opt opt;                   // 操作符：+、-、*、/、% 的类型
    double opd;                // 操作数：常量的值
  };
};

struct ErrMsg {
  PError errcode;
  wchar_t msg[KEYSIZE];
  int pos;
  bool setmsg(const wchar_t *newmsg, PError newerrcode, int newpos = -1) { 
      
      assert(newmsg != NULL);
      clear();
      errcode = newerrcode;
      wcsncpy_s(msg, KEYSIZE, newmsg, KEYSIZE - 1);
      pos = newpos;
      return true;
  }
  bool setmsg(wchar_t newmsg, PError newerrcode, int newpos = -1) {

      clear();
      errcode = newerrcode;
      msg[0] = newmsg;
      pos = newpos;
      return true;
  }
  bool isallright() {
      return errcode == ALLRIGHT;
  }
  void clear(void) {
      memset(msg, 0, sizeof(msg));
      errcode = ALLRIGHT;
      pos = -1;
  }
};

int cmp_func(const void *s1, const void *s2);
int cmp_opt(const void *s1, const void *s2);

class Poly {
public:
  static OptTable opt_table[OPTS];
  static PriTable pri_table[OPTS];
  static FuncTable func_table[FUNCS];
  
  explicit Poly(const wchar_t *newpoly = NULL, int len = 0);
  Poly(Poly &newpoly);
  ~Poly(void) { /* Nothing to do */ };
  bool set_poly(Poly &newpoly);
  bool set_poly(const wchar_t *newpoly, int len);
  const std::wstring &dump(void);
  const wchar_t *get_error_string(void) const;
  double calc(void);
  int nvars(void) const { return _nvars; }
  bool set_vars(const double vararray[], int n);
  void clear(void);
private:
  List<Node> _poly; // 多项式
  List<Vars> _vars; // 变量表 
  List<wchar_t> _str;
  // 实际的变量数，由 set_poly 设置，set_vars 用来检查设置的变量个数是否正确。
  int _nvars;
  ErrMsg _errmsg;

  bool copy_poly(Poly &newpoly);
  void sort_func_table(void);
  void sort_opt_table(void);
  bool do_parse(const wchar_t *newpoly, int len);
  double do_calc(List<Node> &newpoly);
  double do_base_calc(Opt opt, double left, double right);
  double do_func_calc(Func func, double left, double right);
  // 如果是常量，返回常量的值；如果是变量或函数，用tok返回其名字。
  int search_func(const wchar_t *fnuc); // 搜索到了返回值大于或等于 0
  int search_opt(const wchar_t opt);     // 搜索到了返回值大于或等于 0
};

#endif // poly.h
