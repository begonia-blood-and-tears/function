
// DlgProxy.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "function.h"
#include "DlgProxy.h"
#include "functionDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CfunctionDlgAutoProxy

IMPLEMENT_DYNCREATE(CfunctionDlgAutoProxy, CCmdTarget)

CfunctionDlgAutoProxy::CfunctionDlgAutoProxy()
{
	EnableAutomation();

	// To keep the application running as long as an automation
	//	object is active, the constructor calls AfxOleLockApp.
	AfxOleLockApp();

	// Get access to the dialog through the application's
	//  main window pointer.  Set the proxy's internal pointer
	//  to point to the dialog, and set the dialog's back pointer to
	//  this proxy.
	ASSERT_VALID(AfxGetApp()->m_pMainWnd);
	if (AfxGetApp()->m_pMainWnd)
	{
		ASSERT_KINDOF(CfunctionDlg, AfxGetApp()->m_pMainWnd);
		if (AfxGetApp()->m_pMainWnd->IsKindOf(RUNTIME_CLASS(CfunctionDlg)))
		{
			m_pDialog = reinterpret_cast<CfunctionDlg*>(AfxGetApp()->m_pMainWnd);
			m_pDialog->m_pAutoProxy = this;
		}
	}
}

CfunctionDlgAutoProxy::~CfunctionDlgAutoProxy()
{
	// To terminate the application when all objects created with
	// 	with automation, the destructor calls AfxOleUnlockApp.
	//  Among other things, this will destroy the main dialog
	if (m_pDialog != nullptr)
		m_pDialog->m_pAutoProxy = nullptr;
	AfxOleUnlockApp();
}

void CfunctionDlgAutoProxy::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}

BEGIN_MESSAGE_MAP(CfunctionDlgAutoProxy, CCmdTarget)
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CfunctionDlgAutoProxy, CCmdTarget)
END_DISPATCH_MAP()

// Note: we add support for IID_Ifunction to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the
//  dispinterface in the .IDL file.

// {67dba9ee-1b89-4960-b40e-d6a3c0f72c98}
static const IID IID_Ifunction =
{0x67dba9ee,0x1b89,0x4960,{0xb4,0x0e,0xd6,0xa3,0xc0,0xf7,0x2c,0x98}};

BEGIN_INTERFACE_MAP(CfunctionDlgAutoProxy, CCmdTarget)
	INTERFACE_PART(CfunctionDlgAutoProxy, IID_Ifunction, Dispatch)
END_INTERFACE_MAP()

// The IMPLEMENT_OLECREATE2 macro is defined in pch.h of this project
// {f37a9a2f-032c-4996-9092-ed9de5c93090}
IMPLEMENT_OLECREATE2(CfunctionDlgAutoProxy, "function.Application", 0xf37a9a2f,0x032c,0x4996,0x90,0x92,0xed,0x9d,0xe5,0xc9,0x30,0x90)


// CfunctionDlgAutoProxy message handlers
