@echo off
setlocal ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION

set currentdir=
for /F "tokens=*" %%a in ('cd') do (
  set currentdir=%%~na
)
echo 当前工作目录：!currentdir!
set solutionname=!currentdir!.sln
echo 解决方案名字：!solutionname!
if not exist !solutionname! (
  echo 解决方案 [!solutionname!] 不存在
  for /F "tokens=*" %%a in ('dir *.sln /b') do (
    set solutionname=%%a
  )
  echo 解决方案名字是：!solutionname!
)


set dirlist=
call :FindPlatform
echo 需要清理的目录：[!dirlist!]
for %%a in (!dirlist!) do (
  echo %%a
)
if "!dirlist!" == "" exit /b 1
echo 开始清理
echo 清除当前目录中的生成目录
call :CleanSubdirV2 !dirlist!
echo 完毕

rem 依次清理每个项目中的生成目录
for /f %%a in ('dir /a:d /b') do (
  if exist %%a (
    echo 清除 [%%a] 中的生成目录
    cd "%%a"
    call :CleanSubdirV2 !dirlist!
    cd ..
    echo 完毕
  )
)

rem 在一个目录结构复杂的项目里，用下面的方式清理比较方便
forfiles /s /m debug /c "cmd /c if exist @path rd /s /q @path"
forfiles /s /m release /c "cmd /c if exist @path rd /s /q @path"
forfiles /s /m x64 /c "cmd /c if exist @path rd /s /q @path"
forfiles /s /m bin /c "cmd /c if exist @path rd /s /q @path"
forfiles /s /m obj /c "cmd /c if exist @path rd /s /q @path"

goto :EOF

:FindPlatform
set currentdir=
for /F "tokens=*" %%a in ('cd') do (
  set currentdir=%%~na
)
echo 当前工作目录：!currentdir!
set solutionname=!currentdir!.sln
echo 解决方案名字：!solutionname!
if not exist !solutionname! (
  for /F "tokens=*" %%a in ('dir *.sln /b') do (
    set solutionname=%%a
  )
  echo 解决方案名字是：!solutionname!
)
rem for /F "tokens=1 delims=:" %%a in ('findstr /p /l /n SolutionConfigurationPlatforms !solutionname!') do (
  rem set lineskip=%%a
rem )
set startconfig="0"
rem for /F "skip=!lineskip! tokens=*" %%a in (!solutionname!) do (
for /F "tokens=*" %%a in (!solutionname!) do (
  echo "%%a" | findstr /l /p SolutionConfigurationPlatforms > NUL 2>&1
  if "!ERRORLEVEL!"=="0" (
    set startconfig="1"
  ) else if !startconfig!=="1" (
    if "%%a" == "EndGlobalSection" goto :Finish
    for /F "tokens=1,2 delims=^| " %%b in ("%%a") do (
      if "%%c" == "x64" (
        echo !dirlist! | findstr /p /l x64 > NUL 2>&1
        if "!ERRORLEVEL!"=="1" set dirlist=!dirlist!,%%c
      ) else (
        set dirlist=!dirlist!,%%b
      )
    )
  )
)
:Finish

set dirlist=!dirlist!,.vs
rem 删除 dirlist 左侧的空格
for /f "tokens=* delims= " %%a in ("!dirlist!") do set dirlist=%%a
rem 删除 dirlist 左侧的逗号
for /f "tokens=* delims=," %%a in ("!dirlist!") do set dirlist=%%a
call :strlength !dirlist!
echo !length!

goto :EOF

:CleanSubdir
if exist .vs rd /s /q .vs
if exist Debug rd /s /q Debug
if exist Release rd /s /q Release
if exist TestRelease rd /s /q TestRelease
if exist x64 rd /s /q x64
goto :EOF

:CleanSubdirV2
set dl=%*
for %%a in (!dl!) do (
  if exist %%a rd /s /q %%a
)
goto :EOF

:strlength
set length=0
set str=%*
for /L %%a in (0,1,1000) do (
  if "!str:~%%a!" == "" (
    set length=%%a
    goto :END
  )
)
:END
goto :EOF

@echo on
